[![pipeline status](https://gitlab.com/nicolalandro/study-streamsync/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/study-streamsync/-/commits/main) 

# Streamsync study
This repo is for study streamsync

## Run with docker
```
docker-compose up
# localhost:3005
```

## How to dev
* venv
```
python3.10 -m venv venv
source venv/bin/activate.fish
pip install -r requirments.txt
```
* create project
```
streamsync create testapp
```
* ui editor
```
streamsync edit testapp
# go to localhost:3006
```
* run
```
streamsync run testapp
# go to localhost:3005
```
* You can also run as a module
```
python -m streamsync.command_line run testapp
```

## How to test

* setup playwrite
```
pip install pytest-playwright
playwright install
```
* run server
```
streamsync run testapp
```
* run test
```
pytest test
```


# References
* python
* venv
* [Streamsync](https://www.streamsync.cloud/getting-started.html): a lib for made zero UI code with python
* [Playwrite](https://playwright.dev/python/docs/intro): testing lib