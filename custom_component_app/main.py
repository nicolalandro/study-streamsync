import streamsync as ss

def onchange_handler(state, payload):
    state["dna"]["seq"] = payload

initial_state = ss.init_state({
    "my_app": {
        "title": "DNA Sequences"
    },
    "dna": {
        "id": "seqviz",
        "seq": "TTGACGGCTAGCTCAGTCCTAGGTACAGTGCTAGC",
        "name":"J23100"
    }
})
