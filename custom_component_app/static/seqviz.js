var attrs = {
    id: null,
    seq: null,
    name: null
}

function draw_dna_component(){
    new_attr = globalThis.core.evaluateExpression("dna");
    if (attrs.seq !== new_attr.seq) {
        // FOR DEEP COPY
        attrs.seq = new_attr.seq;
        attrs.name = new_attr.name;
        attrs.id = new_attr.id;

        window.seqviz
        .Viewer("seqviz", {
            name: attrs.name,
            seq: attrs.seq,
            style: { height: "50vh", width: "100%" },
            annotations: [{ name: "promoter", start: 0, end: 34, direction: 1 }],
            highlights: [{ start: 0, end: 10 }],
            enzymes: ['Q']
        })
        .render();
    }
}

setTimeout(function () {
    setInterval(draw_dna_component, 1000)
}, 500);