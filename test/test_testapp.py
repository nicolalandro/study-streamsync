import re
from playwright.sync_api import Page, expect


def test_streamsyc_app(page: Page):
    page.goto("http://localhost:3005")

    expect(page.get_by_text("Hello Title (26)")).to_be_visible()

    button_plus = page.get_by_role("button", name="Increment")
    button_plus.click()

    expect(page.get_by_text("Hello Title (27)")).to_be_visible()

    button_less = page.get_by_role("button", name="Decrement")
    button_less.click()

    expect(page.get_by_text("Hello Title (26)")).to_be_visible()