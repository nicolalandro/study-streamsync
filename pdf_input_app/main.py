import streamsync as ss

import base64

def copy_to_clipboard(state):
    state['clipboard']["onclick"] = f'navigator.clipboard.writeText("{state["text"].encode("unicode_escape").decode("utf-8")}")'

def load_pdf(state, payload):
    uploaded_files = payload
    for i, uploaded_file in enumerate(uploaded_files):
        name = uploaded_file.get("name")
        file_data = uploaded_file.get("data")
        path = f"tmp/{name}-{i}.pdf"
        with open(path, "wb") as file_handle:
            file_handle.write(file_data)
        state['iframe_param']['src'] = _iframe_pdf_src(path)

def _change_text(text, state):
    state["text"] = text
    state['clipboard']["onclick"] = f'navigator.clipboard.writeText("{text.encode("unicode_escape").decode("utf-8")}")'

def _iframe_pdf_src(file):
    with open(file, "rb") as f:
        base64_pdf = base64.b64encode(f.read()).decode('utf-8')

    return f"data:application/pdf;base64,{base64_pdf}"

base_text = "# Empty\n Pdf."

initial_state = ss.init_state({
    "my_app": {
        "title": "PDF Reader"
    },
    "text": base_text,
    "iframe_param": {
        "src": "",
        "width":"100%", 
        "height":"500px",
        "type": "application/pdf"
    },
    "clipboard": {
        "onclick": f'navigator.clipboard.writeText("{base_text.encode("unicode_escape").decode("utf-8")}")'
    }
})